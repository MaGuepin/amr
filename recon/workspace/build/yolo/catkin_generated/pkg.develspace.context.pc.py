# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/ubuntu/Documents/amr/recon/workspace/src/yolo/include".split(';') if "/home/ubuntu/Documents/amr/recon/workspace/src/yolo/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;rospy;std_msgs;message_runtime".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lyolo".split(';') if "-lyolo" != "" else []
PROJECT_NAME = "yolo"
PROJECT_SPACE_DIR = "/home/ubuntu/Documents/amr/recon/workspace/devel"
PROJECT_VERSION = "0.0.0"
